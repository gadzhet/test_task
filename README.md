# Environment configuration preparation
```sh
source test_project-openrc_dev.sh
```

# Usage
```sh
python3 test_task.py --help
usage: test_task.py [-h] [--servers] [--images] [--create] [--name NAME]

A simple application for creating a virtual machine in devstack and displaying
a list of all running virtual machines with their ip addresses.

optional arguments:
  -h, --help   show this help message and exit

Options for displaying information.:
  --servers    List servers.
  --images     List images.

Options for create VM.:
  --create     Create a new server.
  --name NAME  Name for a new server. (optional)
```