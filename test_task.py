from argparse import ArgumentParser
from glanceclient import client as gclient
from keystoneauth1 import session
from keystoneauth1.identity import v3
from novaclient import client as nclient
import os
import random
import string


config = {
      "OS_AUTH_URL": os.environ.get('OS_AUTH_URL'),
      "OS_PROJECT_DOMAIN_ID": os.environ.get('OS_PROJECT_DOMAIN_ID'),
      "OS_REGION_NAME": os.environ.get('OS_REGION_NAME'),
      "OS_PROJECT_NAME": os.environ.get('OS_PROJECT_NAME'),
      "OS_USER_DOMAIN_NAME": os.environ.get('OS_USER_DOMAIN_NAME'),
      "OS_IDENTITY_API_VERSION": os.environ.get('OS_IDENTITY_API_VERSION'),
      "OS_INTERFACE": os.environ.get('OS_INTERFACE'),
      "OS_PASSWORD": os.environ.get('OS_PASSWORD'),
      "OS_USERNAME": os.environ.get('OS_USERNAME'),
      "OS_PROJECT_ID": os.environ.get('OS_PROJECT_ID')
}


def prepare_session():
    """
    Prepares the session for further use in the modules.
    :returns: An instance of `keystoneauth1.session.Session`
    """
    auth = v3.Password(auth_url=config.get('OS_AUTH_URL'),
                       username=config.get('OS_USERNAME'),
                       password=config.get('OS_PASSWORD'),
                       project_name=config.get('OS_PROJECT_NAME'),
                       user_domain_id=config.get('OS_PROJECT_DOMAIN_ID'),
                       project_domain_id=config.get('OS_PROJECT_DOMAIN_ID'))
    return session.Session(auth=auth)


def print_servers():
    """
    Print all available servers and their ips
    """
    sess = prepare_session()
    comp = nclient.Client(session=sess, version=2.1)
    for server in comp.servers.list():
        nets = {k1: ' '.join(a.get('addr') for a in v1)
                for k1, v1 in comp.servers.ips(server.id).items()}
        mystring = ' '.join('{}: {}'.format(netname, ips)
                            for netname, ips in nets.items())
        print('Name: {} \t Status: {} \t {}'.format(
            server.name, server.status, mystring))


def print_images():
    """
    Print all available images
    """
    sess = prepare_session()
    gimages = gclient.Client(version='2', session=sess)
    for img in gimages.images.list():
        print(img.get('id'), '\t', img.get('name'), '\t', img.get('status'))


def random_string(string_length=10):
    """
    Generate a random string of fixed length
    :param string_length: Generated string length
    :returns: Ascii letters
    :rtype: str
    """
    letters = string.ascii_letters
    return ''.join(random.choice(letters) for _ in range(string_length))


def create_vm(name=None):
    """
    Well, guys, I made this function as it was written in the technical task.
    I hope that you do not think that I would become hardcore such.
    :param name: name for the virtual machine
    """
    if name == 'auto':
        name = 'auto-' + random_string(8)
    sess = prepare_session()
    comp = nclient.Client(session=sess, version=2.1)
    result_creation = comp.servers.create(
        name=name, image='4a0b32d2-b60d-4f30-8225-47434dcf2b35',
        flavor=1, nics=[{'net-id': '010197be-48c3-43f0-8b26-86487ebdce08'}])
    print('Scheduled creation of instance: {}'.format(result_creation.name))


def parse_args():
    """
    Command line argument parser
    :returns: An instance of `argparse.Namespace`
    """
    cliparse = ArgumentParser(
        description='A simple application for creating a virtual machine in '
                    'devstack and displaying a list of all running virtual '
                    'machines with their ip addresses.')
    show_group = cliparse.add_argument_group(title='Options for displaying '
                                                   'information.')
    show_group.add_argument('--servers', action='store_true',
                            help='List servers.')
    show_group.add_argument('--images', action='store_true',
                            help='List images.')
    create_group = cliparse.add_argument_group(title='Options for create VM.')
    create_group.add_argument('--create', action='store_true',
                              help='Create a new server.')
    create_group.add_argument('--name', default='auto',
                              help='Name for a new server. (optional)')
    return cliparse.parse_args()


if __name__ == '__main__':
    args = parse_args()
    if args.create:
        create_vm(args.name)
    if args.servers:
        print_servers()
    if args.images:
        print_images()
