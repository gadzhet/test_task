keystoneauth1>=3.14.0
python-glanceclient>=2.16.0
python-keystoneclient>=3.19.0
python-novaclient>=14.1.0